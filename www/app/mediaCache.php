<?php
	class mediaCache extends DBObject {
		const collectionName = 'mediaCache';
		public $url;
		public $info;
		public $created;
		
		public function __construct($id=NULL) {			
			$this->created = time();
			parent::__construct($id);
		}	

		public function setInfo($info) {
			$this->info = $info;
			$this->ensureIndex(array('urlHash'=>1),array('unique'=>true));
		}
	
	}
?>
