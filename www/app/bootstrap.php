<?php
	define('BASE_PATH', realpath(dirname(__FILE__)));

	//Autoloader for gif-endec
	spl_autoload_register(function ($class) {
		$filename = BASE_PATH . DIRECTORY_SEPARATOR . str_replace('\\', DIRECTORY_SEPARATOR, $class) . '.php';
		include($filename);
	});
?>
