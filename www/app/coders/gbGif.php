<?php
namespace coders;
use GIFEndec\MemoryStream;
use GIFEndec\Decoder;
use GIFEndec\Frame;

class gbGif extends gbPlugin {
	const TYPE='gif';
	var $frames;
	var $duration;
	var $work; //Work table location
        public function deconstruct($url) {
		$tmp = tempnam('/tmp','gifbpm');

		//DOWNLOADING
		copy($url, $tmp);
		$crc32 = $this->crc32($tmp);

		// Split media apart and save to base media s3.
		$gifStream = new MemoryStream();
		$gifStream->loadFromFile($tmp);
		$gifDecoder = new Decoder($gifStream);
		$duration = 0;
		$frames = 1;
		$this->work = $this->tempdir(); //Define Working Directory

		// DECODING into base media format
		$gifDecoder->decode(function(Frame $frame, $index) {
			$paddedIndex = str_pad($index, 3, '0', STR_PAD_LEFT);
			$frameDuration = $frame->getDuration();
			if ($frameDuration == 0) $frameDuration = 10; //GIF Default
			$this->duration += ($frameDuration * 10); //Magnify by 10 -- translate to ms
			$this->frames++;
			$i = imagecreatefromstring($frame->getStream()->getContents()); //Convert gif to png
			imagepng($i, $this->work."/frame".$paddedIndex.".png"); //Save png as frame in baseMedia
		});

		$info = array( //Define media info
			'url'=>$url,
			'urlHash'=>md5($url),
			'type'=>static::TYPE,
			'size'=>$this->humanFilesize(filesize($tmp)),
			'created'=>time(),
			'frames'=>$this->frames,
			'duration'=>$this->duration,
			'bpm'=>$this->ms2bpm($this->duration),
			'audio'=>false,
			'temp'=>$this->work,
			'crc32'=>$crc32,
		);
		return $this->baseMedia($info); //Save baseMedia format, return information
        }

        public function construct($options) {
                //Load baseMedia from DB
                //Download file from S3
                //Uncompress file into workbench
                //Build based off of $options
                //Upload built file to seperate s3 bucket ( rendered media instead of baseMedia )
        }
}
?>
