<?php
namespace coders;

interface igbPlugin  {
        public function deconstruct($url);  //Break media down into baseMedia
        public function construct($options);  //Build media up from baseMedia
}

class gbPlugin implements igbPlugin {
        var $id;
        var $s3_id;
        var $url;
        var $size;
        var $type;

	public function deconstruct($url) {
	}
	
	public function construct($options) {
	}

	protected function baseMedia($info) {
		if (!$this->work) return false;
		$fp = fopen($this->work."/info.json","w"); //Write info file
		fputs($fp,json_encode($info));
		fclose($fp);

                //Building Base Media
		$command = "tar -C {$this->work} -czvf {$this->work}.tar.gz {$this->work}/*";
		exec($command);
		exec("cp {$this->work}/frame000.png {$this->work}.png");
		exec("rm -rf {$this->work}");
		if (!file_exists("{$this->work}.tar.gz")) return false;
		$info['fileHash'] = $this->crc32($this->work.'.tar.gz');
		return $info;
	}

	//Utilities
	protected function humanFilesize($bytes, $decimals = 2) {
		$size = array('B','kB','MB','GB','TB','PB','EB','ZB','YB');
		$factor = floor((strlen($bytes) - 1) / 3);
		return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$size[$factor];
	}

	protected function bpm2ms($bpm) { //MS in one beat
		return (60000 / $bpm);
	}

	protected function ms2bpm($inc,$lowerLimit=80,$upperLimit=180) {
		while ($inc < $lowerLimit)  $inc *= 2;
		while ($inc > $upperLimit) $inc /= 2;
		return round($inc,2);
	}

	protected function tempdir($prefix='gb') {
		$tempfile=tempnam(sys_get_temp_dir(),'');
		if (file_exists($tempfile))  unlink($tempfile);
		mkdir($tempfile);
		if (is_dir($tempfile)) return $tempfile;
	}

	protected function crc32($path) {
		return md5_file($path);
	}

}
?>
