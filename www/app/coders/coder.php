<?php
	namespace coders;
	class coder {
		static function getByMimeType($mimeType) {
			$mimeParts = explode("/",$mimeType);
			$targetCoder = "\coders\\gb".ucfirst($mimeParts[1]);
			if (class_exists($targetCoder)) {
				return new $targetCoder;
			}
			return false;
		}

		static function deconstruct($url) {
			$coder = null;
			$headers = get_headers($url,true);
			foreach($headers as $key=>$value) if (strtolower($key) == 'content-type') $coder = static::getByMimeType(strtolower($value));
			if (!$coder or is_null($coder)) return 'Invalid content type';
			return $coder->deconstruct($url);
		}


		public function thumbnail($png) { //Create a size appropriate thumbnail of an image
			$command = "/usr/bin/convert -thumbnail 350x350 {$png}";
			exec($command);
		}


		
	}
?>
