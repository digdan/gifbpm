<?php

// Kickstart the framework
$f3=require('lib/base.php'); //Framework Base
require('app/bootstrap.php'); //Autoloader

$f3->set('TITLE','GIFBPM'); //Defaults

if ((float)PCRE_VERSION<7.9)
	trigger_error('PCRE version is out of date');



$f3->config('config.ini'); //Config
$mdb = new MongoClient( $f3->get('mongo.dsn'),array('connect'=>true)); //Mongo DB
$mongoCollection = $f3->get('mongo.collection'); //Mongo Collection
DBObject::setDB( $mdb->$mongoCollection ); //Default collection to Mongo ODM
$s3 = new S3( $f3->get('s3.key'), $f3->get('s3.secret') ); //Connect to S3 Storage

$f3->route('GET /', //HomePage
	function($f3) {
		$f3->set('TITLE','GifBPM - Sync, time and convert GIF WebM FLV videos by Beats Per Minute');
		$f3->set('content','welcome.htm');
		for($i=0;$i<=4;$i++) $slots[$i] = View::instance()->render('media-block.htm');
		$f3->set('slots',$slots);
		echo View::instance()->render('layout.htm');
	}
);
$f3->route('POST /find',
	function($f3) {
		global $s3;
		$f3->set('TITLE','BPM for '.$_POST['url']);
		$f3->set('url',$_POST['url']);
		$urlHash = md5($_POST['url']);
		$c = new mediaCache();
		if (!($record = $c->findOne(array('info.urlHash'=>$urlHash),array('info'=>1)))) {
			$coder = new coders\coder();
			$response = $coder->deconstruct($_POST['url']);
			if (is_string($response)) {
				$f3->set('message', $response);	
			} else {
				$info = $response;
				if ($s3->putObject($s3->inputFile($info['temp'].".tar.gz",false), 'gifbpm',$info['fileHash'].".tar.gz", S3::ACL_PRIVATE)) { //Save to Media Cache
					$coder->thumbnail($info['temp'].".png");
					$s3->putObject($s3->inputFile($info['temp'].".png",false),'gifbpm',$info['fileHash'].".png", S3::ACL_PUBLIC_READ); //Add first frame preview
					$mc = new mediaCache();
					$mc->setInfo($info);
					$mc->update();
					unlink($info['temp'].".tar.gz");
					unlink($info['temp'].".png");
				}
			}
		} else {
			$info = $record['info'];
		}


		$f3->set('info',$info);
		$f3->set('labelInfo', array(
			'size'=>$info['size'],
			'frames'=>$info['frames'],
			'duration'=>$info['duration'],
			'bpm'=>$info['bpm'])
		);
		$f3->set('message', View::instance()->render('labels.htm'));
		$f3->set('embed','<img src="'.$_POST['url'].'" style="max-height:200px;max-width:200px">');
		$f3->set('extra',$extra);

		$f3->set('content','findbpm.htm');
		echo View::instance()->render('layout.htm');
	}
);
$f3->route('GET /cover/@id', function($f3,$params) { //First frame cover
	global $s3;
	header("Content-Type: image/png");
	$target = $f3->get('cdn.host').$params['id'].".png";
	header("Location: ".$target);	
});
$f3->set('ONERROR',function($f3){
	$f3->set('content','error.htm');
	echo View::instance()->render('layout.htm');
});
$f3->run();
